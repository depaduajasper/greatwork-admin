/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { DAEMON } from 'utils/constants';

import Dashboard from 'containers/Dashboard';

import {
  appStart,
  logoutRequest,
} from './actions';
import { isUserAuthenticated } from './selectors';
import saga from './saga';
import reducer from './reducer';


/* eslint-disable react/prefer-stateless-function */
export class App extends React.Component {
  componentDidMount() {
    this.props.appStart();
  }
  /* eslint-disable react/no-children-prop */
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Dashboard} />
      </Switch>
    );
  }
  /* eslint-enable react/no-children-prop */
}
/* eslint-enable react/prefer-stateless-function */
App.propTypes = {
  appStart: PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    appStart: () => dispatch(appStart()),
    logout: () => dispatch(logoutRequest()),
  };
}

const mapStateToProps = createStructuredSelector({
  isAuthenticated: isUserAuthenticated(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withSaga = injectSaga({ key: 'App', saga, mode: DAEMON });
const withReducer = injectReducer({ key: 'app', reducer });

export default withRouter(compose(
  withSaga,
  withReducer,
  withConnect,
)(App));
