import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    font-size: 16px;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'LatoWeb', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
  }

  #app {
    background-color: #fff;
    min-height: 100%;
    min-width: 100%;
    display: flex;
    justify-content: center;
  }

  #app > * {
    ${'' /* min-width: 375px;
    max-width: 1366px; */}
  }

  p,
  label {
    line-height: 1.5em;
  }

  /* Make use of these when needed! -- ralph*/
  .scrollbar-custom::-webkit-scrollbar-track
  {
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 5px;
    background-color: #F5F5F5;
  }

  .scrollbar-custom::-webkit-scrollbar
  {
    width: 4px;
    height: 4px;
    background-color: #F5F5F5;
  }

  .scrollbar-custom::-webkit-scrollbar-thumb
  {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
  }
`;
