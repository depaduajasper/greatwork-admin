
/*
 *
 * Main actions
 *
 */

import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGIN_USER_DETAILS_SAVE,
  APP_START,
} from './constants';

export function appStart() {
  return {
    type: APP_START,
  };
}

export function loginRequest(credentials, rememberSession) {
  return {
    type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    rememberSession,
    credentials,
  };
}

export function loginSuccess() {
  return {
    type: LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
  };
}

export function loginUserDetailsSave(user, accessToken, refreshToken, expiresIn) {
  return {
    type: LOGIN_USER_DETAILS_SAVE,
    user,
    accessToken,
    refreshToken,
    expiresIn,
  };
}

export function loginFailure(error) {
  return {
    type: LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    error,
  };
}

export function logoutRequest() {
  return {
    type: LOGOUT_REQUEST,
    isFetching: false,
    isAuthenticated: false,
  };
}
