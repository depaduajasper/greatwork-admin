const requestHeaders = {
  method: 'GET',
  headers: new Headers({
    Accept: 'application/json',
  }),
};
const fetchPromiseToJson = (response) => response.json();

const apiFetch = ({
  url, method, dataType, token,
}, data) => {
  const config = {
    method: method || 'GET',
    headers: new Headers({
      Accept: 'application/json',
    }),
  };
  if (token) {
    config.headers.append('Authorization', `Bearer ${token}`);
  }
  if (data) {
    const type = dataType || 'application/x-www-form-urlencoded';
    config.contentType = type;
    switch (type) {
      case 'multipart/form-data':
        config.body = new FormData(data);
        break;
      case 'application/x-www-form-urlencoded':
        config.body = new URLSearchParams(data);
        break;
      default:
        throw new Error('Unsupported data type supplied to apiFetch');
    }
  }
  return fetch(`${process.env.API_URL}${url}`, config).then(fetchPromiseToJson);
};

const parseToken = (token) => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
};

const serializeUserObject = (obj) => window.btoa(JSON.stringify(obj));

const parseUserObject = (string) => JSON.parse(window.atob(string));

export {
  requestHeaders,
  fetchPromiseToJson,
  apiFetch,
  parseToken,
  serializeUserObject,
  parseUserObject,
};
