# Mnemosyne [Frontend]
## Project Description
As PRIME continues to strive towards its mission: to provide real estate at the highest standard and deliver innovative and effective solutions that bring success to its clients, there is a need to consolidate and aggregate all information so that the company's employees are on the same page. Currently, this takes a lot of time and effort on behalf of HR and the respective departments who make sure that the company is aligned and its employees are working together.

Project Mnemosyne, part of the initiatives of the IT Department, aims to facilitate cross-departmental learning and information, as well as decrease onboarding and collaboration friction by centralizing core non-confidential information in a repository. For interactivity's sake, Mnemosyne uses videos where applicable to increase user engagement.

This repository hosts the frontend of Project Mnemosyne, and is made out of React, Redux and relevant/associated tech.

## Project Status
This project is currently marked as a **Kickstart P1S4** project.
* Kickstart projects denote new or existing projects that are currently in either of the following states:
  * Bootstrapping
  * Brainstorming
  * Reassessment
  * Proof-of-concept
* P1 projects are moonshot/experimental initiatives that will be promoted to higher-purpose levels once they fulfill their initial objectives.
* S4 projects are expected to resolve or deliver their respective objectives in 2 weeks to a month.

Target delivery date: **End of 2017**

## Project Organization
__Project Manager:__ Ralph Sto. Domingo <ralph.sto.domingo@primephilippines.com>

__Committee Members:__
1. Isaiah de Castro <isaiah.de.castro@primephilippines.com>
2. *Romel Dellosa* [pending acknowledgement]

__Implementation Team Members:__
1. Angela Moreno
2. Cyriel Basilio
3. Jasper John de Padua
4. Joshua Fabian
5. Julius Dejon
6. Ken Ryan Labso
7. Rod Reynold Real

## Getting Started
* clone the repo
* `npm install`
* `npm start`
* Copy the `.env.example` file, rename into `.env`, and edit as needed to suit your environment
