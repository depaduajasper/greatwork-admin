import { createSelector } from 'reselect';

const selectRoute = (state) => state.get('route');

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

/**
 * Direct selector to the loginModal state domain
 */
const selectAuthDomain = () => (state) => state.get('app').auth;

/**
 * Default selector used by LoginModal
 */

const makeSelectAuth = () => createSelector(
  selectAuthDomain(),
  (substate) => substate.toJS()
);

const isUserAuthenticated = () => createSelector(
  selectAuthDomain(),
  (substate) => substate.get('isAuthenticated')
);

const isAuthFetching = () => createSelector(
  selectAuthDomain(),
  (substate) => substate.get('isFetching')
);

const getUserObj = () => createSelector(
  selectAuthDomain(),
  (substate) => substate.get('user')
);

const getUserName = () => createSelector(
  getUserObj(),
  (substate) => (substate.first_name && substate.last_name) && `${substate.first_name} ${substate.last_name}`,
);

const getUserPosition = () => createSelector(
  getUserObj(),
  (substate) => substate.position,
);

const getUserUsername = () => createSelector(
  getUserObj(),
  (substate) => substate.user_name,
);

const getUserDepartment = () => createSelector(
  getUserObj(),
  (substate) => substate.department,
);

export {
  makeSelectLocation,

  selectAuthDomain,
  makeSelectAuth,
  isUserAuthenticated,
  isAuthFetching,
  getUserObj,
  getUserName,
  getUserPosition,
  getUserUsername,
  getUserDepartment,
};
