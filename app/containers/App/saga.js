/**
 * Saga for the api authenticator.
 */

import { call, put, take, fork, cancel } from 'redux-saga/effects';
import { apiFetch, serializeUserObject, parseUserObject } from 'common/api';
import showNotification, { getErrorNotif } from 'common/notification';
import { computeExpiresIn } from 'common/utilities';
import {
  getUserObject,
  removeUserObject,
  setUserObject,
  getAuthToken,
  getTokensAndExpiresIn,
  setTokensAndExpiresIn,
  removeTokensAndExpiresIn,
} from 'common/localstorage';
import { LOGIN_REQUEST, LOGOUT_REQUEST, APP_START } from './constants';
import { loginSuccess, loginUserDetailsSave, loginFailure } from './actions';

export function* logoutCleanUp() {
  yield call(removeTokensAndExpiresIn);
  yield put(loginUserDetailsSave({}));
  yield call(removeUserObject);
  yield call(showNotification, {
    title: 'You have logged out successfully.',
    message: 'See you again soon!',
  });
}

// We fetch from the db the user object if no such encoded string exists in localStorage. Else, we
// parse it and rehydrate the user state.
export function* getUserDetails() {
  yield put(loginSuccess());
  try {
    const { token, refresh, expires } = yield call(getTokensAndExpiresIn);
    const userString = yield call(getUserObject);
    let userDetails;
    if (!userString) {
      userDetails = yield call(apiFetch, {
        url: '/user/profile',
        method: 'GET',
        token,
      });
      const serializedUser = yield call(serializeUserObject, userDetails.data);
      yield call(setUserObject, serializedUser);
      // console.log('user object missing in storage, fetched from db', userDetails, userString);
    } else {
      userDetails = yield call(parseUserObject, userString);
      // console.log('user object found in storage, parsed.', userDetails, userString);
    }
    yield put(loginUserDetailsSave(userDetails, token, refresh, expires));
  } catch (error) {
    if (error.error === 'token_expired') {
      yield call(showNotification, getErrorNotif('Token expired. Please log-in again.'));
      yield put(loginFailure(error.error));
    } else if (error.error) {
      yield call(showNotification, getErrorNotif(`An error has occurred: ${error.error}`));
      yield put(loginFailure(error.error));
    } else {
      yield call(showNotification, getErrorNotif(`An error has occurred: ${JSON.stringify(error)}`));
      yield put(loginFailure(error));
    }
    yield call(logoutCleanUp);
  }
}

export function* waitForLogoutAndCleanup(getUserDetailsTask) {
  yield take(LOGOUT_REQUEST);
  yield cancel(getUserDetailsTask);
  // Remove token from storage, user in store, show notif
  yield call(logoutCleanUp);
}

// And if this makes you wonder too, separating this helps us to test the
// auth flow because apparently redux-saga-test-plan's provider mocking does
// not support endless loops.
export function* authOnce() {
  try {
    // check if token exists...
    const storedToken = yield call(getAuthToken);
    let getUserDetailsTask;
    if (storedToken == null) {
      const action = yield take(LOGIN_REQUEST);
      const response = yield call(apiFetch, {
        url: '/oauth/token',
        method: 'POST',
        dataType: 'application/x-www-form-urlencoded',
      }, {
        ...action.credentials,
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
        grant_type: 'password',
        scope: '',
      });
      if (response.error) {
        yield put(loginFailure(response.error));
        if (response.error === 'invalid_credentials') {
          yield call(showNotification, getErrorNotif('The username/password combination is not correct or user does not exist.'));
        } else {
          yield call(showNotification, getErrorNotif(response.message));
        }
      } else {
        yield call(showNotification, {
          title: 'You have logged in successfully.',
          message: 'Nice to see you!',
        });
        yield call(setTokensAndExpiresIn, response.access_token, action.rememberSession ? response.refresh_token : null, action.rememberSession ? computeExpiresIn(response.expires_in) : null);
      }
      if (!response.error) {
        getUserDetailsTask = yield fork(getUserDetails);
        yield call(waitForLogoutAndCleanup, getUserDetailsTask);
      }
    } else {
      getUserDetailsTask = yield fork(getUserDetails);
      yield call(waitForLogoutAndCleanup, getUserDetailsTask);
    }
  } catch (error) {
    yield call(showNotification, getErrorNotif(error));
  }
}

// ...and our run-once auth flow will be `yield`ed here.
export function* authorize() {
  yield take(APP_START);
  // auth lifecycle loop.
  while (true) { // eslint-disable-line no-constant-condition
    try {
      yield call(authOnce);
    } catch (error) {
      yield call(showNotification, getErrorNotif(error));
    }
  }
}

export default function* rootSaga() {
  yield call(authorize);
}
