import moment from 'moment';
import gradients from './gradients.json';

const numberFormat = (num) => // consider putting in utilities
  Number(parseFloat(num).toFixed(2)).toLocaleString('en', {
    minimumFractionDigits: 2,
  });

const computeExpiresIn = (expiresIn) => {
  // expiresIn is expected to be in seconds.
  const now = moment();
  return now.clone().add(expiresIn, 's').toString();
};

function getRandomInt(min, max) {
  return Math.floor(Math.random() * ((max - min) + 1)) + min;
}

function randomGradient() {
  const selectedGradient = gradients[getRandomInt(0, gradients.length)];
  return selectedGradient;
}

function hexToRGB(hex) {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);
  const rgb = `${r}, ${g}, ${b}`;
  return rgb;
}

function randomGradientHex() {
  const gradient = randomGradient();
  return gradient.map((val) => hexToRGB(val));
}

const computeVideoDuration = (input) => {
  let num = input;
  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  let durationString = '';
  if (num >= 3600) {
    hours = Math.floor(num / 3600);
    num %= 3600;
  }
  minutes = Math.floor(num / 60);
  seconds = num % 60;

  durationString = hours <= 0 ?
    `${minutes > 9 ? '' : '0'}${minutes}:${seconds > 9 ? '' : '0'}${seconds}` :
    `${hours}:${minutes > 9 ? '' : '0'}${minutes}:${seconds > 9 ? '' : '0'}${seconds}`;
  return durationString;
};


export {
  numberFormat,
  computeExpiresIn,
  randomGradient,
  randomGradientHex,
  hexToRGB,
  computeVideoDuration,
};
