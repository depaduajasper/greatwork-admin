import React from 'react';
import { notification } from 'antd';
import FontAwesome from 'react-fontawesome';

const showNotification = ({
  title, message, icon, placement, button, duration,
}) => {
  const key = performance.now();
  notification.open({
    placement: placement || 'topRight',
    message: title,
    description: message,
    icon: icon || null,
    btn: button || null,
    key,
    duration: duration || 4.5,
    className: 'yutago-notification',
  });
  return {
    close: () => notification.close(key),
  };
};

const getErrorNotif = (error) => ({
  title: 'Uh-oh, looks like we have a problem here...',
  message: `${error}`,
  icon: <FontAwesome name="exclamation-triangle" className="accent-color" />,
});

export default showNotification;
export {
  getErrorNotif,
};
