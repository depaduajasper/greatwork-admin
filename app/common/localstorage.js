function getAuthToken() {
  return window.localStorage.getItem('token');
}
function getUserObject() {
  return window.localStorage.getItem('user');
}
function getRefreshToken() {
  const ret = window.localStorage.getItem('refresh');
  return ret !== 'null' ? ret : null;
}
function getExpiresIn() {
  const ret = window.localStorage.getItem('expires_in');
  return ret !== 'null' ? ret : null;
}

function setAuthToken(token) {
  return window.localStorage.setItem('token', token);
}
function setUserObject(user) {
  return window.localStorage.setItem('user', user);
}
function setRefreshToken(token) {
  return window.localStorage.setItem('refresh', token);
}
function setExpiresIn(millis) {
  return window.localStorage.setItem('expires_in', millis);
}

function removeUserObject() {
  return window.localStorage.removeItem('user');
}
function removeAuthToken() {
  return window.localStorage.removeItem('token');
}
function removeRefreshToken() {
  return window.localStorage.removeItem('refresh');
}
function removeExpiresIn() {
  return window.localStorage.removeItem('expires_in');
}

function getTokensAndExpiresIn() {
  const token = getAuthToken();
  const refresh = getRefreshToken();
  const expires = getExpiresIn();
  return {
    token,
    refresh,
    expires,
  };
}

function setTokensAndExpiresIn(accessToken, refreshToken, expiresIn) {
  setAuthToken(accessToken);
  setRefreshToken(refreshToken);
  setExpiresIn(expiresIn);
}

function removeTokensAndExpiresIn() {
  removeAuthToken();
  removeRefreshToken();
  removeExpiresIn();
}

export {
  getUserObject,
  getAuthToken,
  getRefreshToken,
  getExpiresIn,
  setUserObject,
  setAuthToken,
  setRefreshToken,
  setExpiresIn,
  removeUserObject,
  removeAuthToken,
  removeRefreshToken,
  removeExpiresIn,
  getTokensAndExpiresIn,
  setTokensAndExpiresIn,
  removeTokensAndExpiresIn,
};
