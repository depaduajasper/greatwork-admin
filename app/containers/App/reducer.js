/*
 *
 * Dashboard reducer
 *
 */

import { fromJS } from 'immutable';
import { combineReducers } from 'redux';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGIN_USER_DETAILS_SAVE,
} from './constants';

const initialState = fromJS({
  isAuthenticated: false,
  isFetching: false,
  user: {},
  access_token: null,
  refresh_token: null,
  expires_in: null,
});

export function authReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT_REQUEST:
    case LOGIN_REQUEST:
    case LOGIN_SUCCESS:
    case LOGIN_FAILURE:
      return state.set('isAuthenticated', action.isAuthenticated)
        .set('isFetching', action.isFetching);
    case LOGIN_USER_DETAILS_SAVE:
      return state
        .set('user', action.user)
        .set('access_token', action.accessToken)
        .set('refresh_token', action.refreshToken)
        .set('expires_in', action.expiresIn);
    default:
      return state;
  }
}

export default combineReducers({
  auth: authReducer,
});
